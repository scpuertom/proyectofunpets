from django.db import models
from .user import User
from .post import Post
from .typereaction import TypeReaction

class Reaction(models.Model):
    reaction_id = models.BigAutoField(primary_key=True)
    reaction_creation_date=models.DateTimeField()
    post_id = models.ForeignKey(Post,related_name="post", on_delete=models.CASCADE)
    user_id = models.ForeignKey(User,related_name="user",on_delete=models.CASCADE)
    typereaction_id=models.OneToOneField(TypeReaction,related_name="typereaction", on_delete=models.CASCADE)